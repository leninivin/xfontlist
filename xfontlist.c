#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <X11/Xlib.h>

void fatal(char* msg)
{
	fprintf(stderr, "FATAL: %s. Aborting.\n", msg);
	exit(1);
}

void error(char* msg)
{
	fprintf(stderr, "ERROR: %s.\n", msg);
}

int main(int argc, char **argv)
{
	int DEBUG = (getenv("DEBUG") != NULL);

	char **fontlist  = NULL;
	char *pattern    = "*-*-*-*-*-*-*-*-*-*-*-*-*";
	int maxnames     = 0xFFFF;
	int *nr_returned = malloc(sizeof(int));
	if(nr_returned == NULL) fatal("Cannot allocate memory");
	// For debug
	int max_pad  = 0;
	char *padstr = NULL;

	Display *display = NULL;
	display = XOpenDisplay(NULL);
	if(display == NULL) fatal("Cannot open display");

	if(DEBUG)
	{
		int screen = -1;
		screen = DefaultScreen(display);
		if(screen == -1) error("Cannot obtain screen");
		else printf("Screen is: %d\n", screen);

		char *vendor = NULL;
		vendor = ServerVendor(display);
		if(vendor == NULL) error("Cannot obtain vendor");
		else printf("Vendor is: %s\n", vendor);

		char *dstring = NULL;
		dstring = DisplayString(display);
		if(dstring == NULL) error("Cannot obtain display string");
		else printf("Display string is: %s\n", dstring);

		printf("\nFont list (capped to %d):\n\n", maxnames);
	}

	fontlist = XListFonts(display, pattern, maxnames, nr_returned);

	if(DEBUG)
	{
		max_pad = (int)floor(log10(*nr_returned)) + 1;
		padstr = malloc((max_pad + 1) * sizeof(char));
		if(padstr == NULL) fatal("Cannot allocate memory");
	}

	int i;
	for(i = 0; i < *nr_returned; i++)
	{
		if(DEBUG)
		{
			int pad = (int)floor(log10(i+1)) + 1;
			memset(padstr, ' ', max_pad + 1);
			padstr[max_pad - pad] = '\0';
			printf("[%s%d/%d] '", padstr, i+1, *nr_returned);
		}
		printf("%s%s", fontlist[i], (DEBUG?"'\n":"\n"));
	}

	free(nr_returned);
	if(DEBUG) free(padstr);

	return 0;
}
