LDFLAGS=-lm -lX11
BIN=xfontlist

all: $(BIN)

%: %.c

clean:
	rm -f *.o $(BIN)

.PHONY: clean
